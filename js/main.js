$(document).ready(function () {
    // Mobile Menu Change Icon
    $('.user-mobile').click(function () {
        $('.menu-mobile-container div').not($(this)).removeClass('active');
        $(this).toggleClass("active");
        changeIcons();
    });
    $('.search-mobile').click(function () {
        $('.menu-mobile-container div').not($(this)).removeClass('active');
        $(this).toggleClass("active");
        changeIcons();
    });
    $('.menu-mobile').click(function () {
        $('.menu-mobile-container div').not($(this)).removeClass('active');
        $(this).toggleClass("active");
        $('header').toggleClass('active');
        changeIcons();
    });
    function changeIcons() {
        if ($('.menu-mobile').hasClass("active")) {
            $('.menu-mobile').find($(".fa")).removeClass('fa-bars');
            $('.menu-mobile').find($(".fa")).addClass('fa-times');
            $('#mainMenu').addClass('active');
        }
        else {
            $('.menu-mobile').find($(".fa")).addClass('fa-bars');
            $('.menu-mobile').find($(".fa")).removeClass('fa-times');
            $('#mainMenu').removeClass('active');
        }
        if ($('.user-mobile').hasClass("active")) {
            $('.user-mobile').find($(".fa")).removeClass('fa-user');
            $('.user-mobile').find($(".fa")).addClass('fa-times');
            $('.mobile-login-in').addClass('active');
        }
        else {
            $('.user-mobile').find($(".fa")).addClass('fa-user');
            $('.user-mobile').find($(".fa")).removeClass('fa-times');
            $('.mobile-login-in').removeClass('active');
        }
        if ($('.search-mobile').hasClass("active")) {
            $('.search-mobile').find($(".fa")).removeClass('fa-search');
            $('.search-mobile').find($(".fa")).addClass('fa-times');
            $('.mobile-search-container').addClass('active');
        }
        else {
            $('.search-mobile').find($(".fa")).addClass('fa-search');
            $('.search-mobile').find($(".fa")).removeClass('fa-times');
            $('.mobile-search-container').removeClass('active');
        }

    }

    // Custom dropdown 
    $(".header-select .dropdown-menu li a").on('click', function () {
        var text = $(this).html();
        $(".header-select a span").html(text);
        $(".header-select .dropdown-menu ul").hide();
    });

    // Custom dropdown 
    $('#dl-menu').dlmenu({
        animationClasses: { classin: 'dl-animate-in-2', classout: 'dl-animate-out-2' }
    });

});
